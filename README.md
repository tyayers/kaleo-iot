# Kaleo Connected Car IoT API Demo

This demo uses several Google Cloud services to demonstrate an end-to-end IoT connected car use-case.  A car simulator app lets the user drive around a 3d city, while telemetric data is ingested via APIs that are published in Apigee, and published to Cloud IoT Core.  The data from there are pushed to a Pub/Sub topic, which is then sent to DataProc and stored in BigQuery.  The advantage is that the data can be processed and agrigated before being stored in BigQuery, where queries and analytics can be done.

The data currently ingested include:
* Speed
* Tire wear
* Collisions detected
* Latitude
* Longitude
* Timestamp

Once the data are in BigQuery, they are then queried from 2 additional apps: a Driver Dashboard app (theoretically from a 3rd party developer) which shows the driver his/her current driving statistics, and a Damage Leaderboard app which shows the "winner" of who has had the most collisions until now.

## Architecture

Cloud Services used:
* [Apigee API Management](https://cloud.google.com/apigee/)
* [GCP Cloud IoT Core](https://cloud.google.com/iot-core/)
* [GCP Cloud Pub/Sub](https://cloud.google.com/pubsub/)
* [GCP DataProc](https://cloud.google.com/dataproc/)
* [GCP BigQuery](https://cloud.google.com/bigquery/)
* [Firebase](https://firebase.google.com/)

<img src="img/kaleo-iot-architecture.png" height="600" />

## Apps

### Grand IoT Auto

The Grand IoT Auto app is a 3d browser simulator where you can drive around, and feed data into the cloud.  Don't forget to set your VIN (Vehicle Identification Number) first in the upper-left corner, so that your data are correctly linked.

[App Link](https://kaleoiot.firebaseapp.com/)

<a href="https://kaleoiot.firebaseapp.com/" target="_blank"><img src="img/grand-iot-auto.png" height="400" /></a>

### Driver Dashboard

The Driver Dashboard app pulls data from the connected car API for a particular VIN - add yours to view your data, in real-time.  The more collisions you have, the more points you lose on your Driver Score.  But on the flip side, you climb in the Collision Ranking on the leaderboard (see next app)!  

[App Link](https://kaleoiot.firebaseapp.com/driver-dash.html)

<a href="https://kaleoiot.firebaseapp.com/driver-dash.html" target="_blank"><img src="img/dashboard.png" height="400" /></a>

### Collisions Leaderboard

Accept the challenge, and rise up in the Collisions Leaderboard!  Here you can see the top 10 drivers with the most collisions, pulled in real-time through the connected car API and BigQuery.

[App Link](https://kaleoiot.firebaseapp.com/leaderboard.html)

<a href="https://kaleoiot.firebaseapp.com/leaderboard.html" target="_blank"><img src="img/leaderboard.png" height="400" /></a>

### Connected Car Developer Portal

[App Link](https://emea-poc13-connectedcar.apigee.io/)

<a href="https://emea-poc13-kaleomedia.apigee.io/" target="_blank"><img src="img/portal.png" height="400" /></a>

The portal is the single point of contact for all developers to browse and subscribe to the APIs.  Monetization of premium content will soon be added, so that developers can also buy access to specific shows that are especially valuable and popular.

## Deployment

### Frontend
The frontend apps are all single page apps, and can easily be deployed on Firebase (or any static hosting).  For Firebase, just install the [Firebase CLI](https://firebase.google.com/docs/cli), and run `firebase serve` in the frontend directory to serve locally, or `firebase deploy` to deploy to a firebase project.

### Backend
The backend service can be deployed to any docker environment, including [Cloud Run](https://cloud.google.com/run/) serverless container hosting.

You will need to deploy your IoT Core private key (rsa_private.pem) and BigQuery private key (privatekey.json) in the backend service (cardata-service) directory, so that the microservice can talk to IoT Core and BigQuery.

### GCP IoT Core / Pub/Sub / DataProc / BigQuery
These components were deployed according to the docs here: [IoT Core Quickstart](https://cloud.google.com/iot/docs/quickstart).  The integration in the backend to the components is straightforward, if you deploy the components yourself.

## Credits
AWESOME 3d CSS Car: https://codepen.io/Beclamide/pen/bNyxVz

Leaderboard: https://codepen.io/uenify/pen/bxpNjY

