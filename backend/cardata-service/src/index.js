// Load environment variables
require('dotenv').config();

const jwt = require('jsonwebtoken');
const mqtt = require('mqtt');
const request = require('request');
const express = require('express');
const cors = require('cors');
const app = express();
app.use(cors({ origin: true }));
app.use(express.json({limit: '50mb'}));
const {BigQuery} = require('@google-cloud/bigquery');
const options = {
    keyFilename: 'privatekey.json',
    projectId: 'tyler-240211',
};
const bigquery = new BigQuery(options);

const argv = {
    projectId: "tyler-240211",
    region: "europe-west1",
    registryId: "tyler-reg15",
    deviceId: "car1",
    messageType: "events",
    privateKeyFile: "rsa_private.pem",
    algorithm: "RS256",
    tokenExpMins: 20,
    mqttBridgeHostname: "mqtt.googleapis.com",
    mqttBridgePort: 8883,
    httpBridgeAddress: "cloudiotdevice.googleapis.com"
};

// const mqttClientId = `projects/${argv.projectId}/locations/${argv.region}/registries/${argv.registryId}/devices/${argv.deviceId}`;

// const connectionArgs = {
//     host: argv.mqttBridgeHostname,
//     port: argv.mqttBridgePort,
//     clientId: mqttClientId,
//     username: 'unused',
//     password: createJwt(argv.projectId, argv.privateKeyFile, argv.algorithm),
//     protocol: 'mqtts',
//     secureProtocol: 'TLSv1_2_method',
// };

// Create a client, and connect to the Google MQTT bridge.
// const iatTime = parseInt(Date.now() / 1000);
// const client = mqtt.connect(connectionArgs);
// const mqttTopic = `/devices/${argv.deviceId}/${argv.messageType}`;

// var connected = false;

// client.on('connect', success => {
//     console.log('connect');
//     if (!success) {
//         console.log('Client not connected...');
//         connected = false;
//     } else {
//         connected = true;
//     }
// });

// client.on('close', () => {
//     connected = false;
//     console.log('close');
// });

// client.on('error', err => {
//     connected = false;
//     console.log('error', err);

//     setTimeout(function() {
//         console.log("Refreshing token..");
//         // Refresh token
//         connectionArgs.password = createJwt(argv.projectId, argv.privateKeyFile, argv.algorithm);
//         const client = mqtt.connect(connectionArgs);
//     }, 1000);
// });

// client.on('message', (topic, message) => {
//     console.log(
//         'message received: ',
//         Buffer.from(message, 'base64').toString('ascii')
//     );
// });

// client.on('packetsend', () => {
//     // Note: logging packet send is very verbose
// });

app.get('/car-data/collision-leaders', async(req, res) => {
    const collisionsquery = "SELECT COUNT(collisiondetected) as collisions, vin FROM `tyler-240211.ConnectedCarDataset.CarData` WHERE collisiondetected=true GROUP BY vin ORDER BY collisions DESC";

    const collisionoptions = {
      query: collisionsquery,
      // Location must match that of the dataset(s) referenced in the query.
      location: 'EU',
    };

    // Run the query as a job
    const [collisionsjob] = await bigquery.createQueryJob(collisionoptions);

    // Wait for the query to finish
    var [collisionsrows] = await collisionsjob.getQueryResults();

    for (var i=0; i<collisionsrows.length; i++) {
        var row = collisionsrows[i];
        row["driverscore"] = (100 / (row["collisions"] + 1)).toFixed(2);
    }

    res.end(JSON.stringify(collisionsrows));
});

app.get('/car-data/:vin', async(req, res) => {

    var vin = req.params.vin;

    const collisionsquery = "SELECT COUNT(collisiondetected) as collisions FROM `tyler-240211.ConnectedCarDataset.CarData` WHERE collisiondetected=true AND vin='" + vin + "'";
    const tirewearquery = "SELECT SUM(tirewear) as tirewear FROM `tyler-240211.ConnectedCarDataset.CarData` WHERE vin='" + vin + "'";

    // For all options, see https://cloud.google.com/bigquery/docs/reference/rest/v2/jobs/query
    const collisionoptions = {
      query: collisionsquery,
      // Location must match that of the dataset(s) referenced in the query.
      location: 'EU',
    };

    const tirewearoptions = {
        query: tirewearquery,
        // Location must match that of the dataset(s) referenced in the query.
        location: 'EU',
      };

    // Run the query as a job
    const [collisionsjob] = await bigquery.createQueryJob(collisionoptions);

    // Wait for the query to finish
    const [collisionsrows] = await collisionsjob.getQueryResults();

    var result = {
        vin: vin,
        collisions: 0,
        driverscore: 100,
        tirewear: 0
    };

    if (collisionsrows.length > 0) {
        result["collisions"] = collisionsrows[0].collisions;
    }

    result["driverscore"] = (100 / (result["collisions"] + 1)).toFixed(2);

    const [tirejob] = await bigquery.createQueryJob(tirewearoptions);
    const [tirerows] = await tirejob.getQueryResults();

    if (tirerows.length > 0) {
        result["tirewear"] = parseFloat(tirerows[0].tirewear).toFixed(2);
    }

    res.end(JSON.stringify(result));
});

app.put('/car-data/:vin', (req, res) => {

    const devicePath = `projects/${argv.projectId}/locations/${argv.region}/registries/${argv.registryId}/devices/${argv.deviceId}`;

    // The request path, set accordingly depending on the message type.
    const pathSuffix = ':publishEvent';
    const urlBase = `https://${argv.httpBridgeAddress}/v1/${devicePath}`;
    const url = `${urlBase}${pathSuffix}`;

    console.log(JSON.stringify(req.body));

    const binaryData = Buffer.from(JSON.stringify(req.body)).toString('base64');
    const postData = { binary_data: binaryData };

    const authToken = createJwt(
        argv.projectId,
        process.env.IOTPRIVATEKEY,
        argv.algorithm
      );

    const options = {
        url: url,
        headers: {
            authorization: `Bearer ${authToken}`,
            'content-type': 'application/json',
            'cache-control': 'no-cache',
        },
        body: postData,
        json: true,
        method: 'POST',
        retries: 5,
        shouldRetryFn: function(incomingHttpMessage) {
            return incomingHttpMessage.statusMessage !== 'OK';
        },
    };

    request(options, (error, response) => {
        if (error) {
            console.error('Received error: ', error);
            res.end(JSON.stringify({result: "Publish error: " + error}));
        } else if (response.body.error) {
            console.error('Received error: ' + JSON.stringify(response.body.error));
            res.end(JSON.stringify({result: "Publish error: " + esponse.body.error}));
        } else {
            console.log('Message sent.');
            res.end(JSON.stringify({result: "Publish successful."}));
        }
    });
});


app.put('/car-data-mqtt', (req, res) => {

    const mqttClientId = `projects/${argv.projectId}/locations/${argv.region}/registries/${argv.registryId}/devices/${argv.deviceId}`;

    const connectionArgs = {
        host: argv.mqttBridgeHostname,
        port: argv.mqttBridgePort,
        clientId: mqttClientId,
        username: 'unused',
        password: createJwt(argv.projectId, process.env.IOTPRIVATEKEY, argv.algorithm),
        protocol: 'mqtts',
        secureProtocol: 'TLSv1_2_method',
    };

    // Create a client, and connect to the Google MQTT bridge.
    const iatTime = parseInt(Date.now() / 1000);
    const client = mqtt.connect(connectionArgs);
    const mqttTopic = `/devices/${argv.deviceId}/${argv.messageType}`;
    console.log("attempting connect..");

    setTimeout(function() {
        console.log("HELLO!!");
    }, 1000);

    client.on('connect', success => {
        console.log('connect');
        if (!success) {
            console.log('Client not connected...');
            res.end(JSON.stringify({result: "Publish error: could not connect."}));
        } 
        else {
            //publishAsync(mqttTopic, client, iatTime, 1, 1, connectionArgs);
            //const payload = `${argv.registryId}/${argv.deviceId}-payload-${iatTime}`;
            const payload = JSON.stringify(req.body);

            // Publish "payload" to the MQTT topic. qos=1 means at least once delivery.
            console.log('Publishing message:', payload);
            client.publish(mqttTopic, payload, {qos: 1}, err => {
            if (!err) {
                console.log("publish successful");
                res.end(JSON.stringify({result: "Publish successful."}));
            }
            else
                console.log("publish error: " + err);
                res.end(JSON.stringify({result: "Publish error: " + err}));
            });          
        }
    });  
    
    client.on('close', () => {
        console.log('close');
    });

    client.on('error', err => {
        console.log('error', err);
    });

    client.on('message', (topic, message) => {
        console.log(
            'message received: ',
            Buffer.from(message, 'base64').toString('ascii')
        );
    });    

    // if (connected) {
    //     console.log("Connected to IoT Core, so sending message..");
        
    //     const payload = `${argv.registryId}/${argv.deviceId}-payload-${iatTime}`;

    //     console.log('Publishing message:', payload);
    //     client.publish(mqttTopic, payload, {qos: 1}, err => {
    //     if (!err) {
    //         console.log("publish successful");
    //         res.end("publish successful");
    //     }
    //     else
    //         console.log("publish error: " + err);
    //         res.end("publish error: " + err);
    //     });  
    // }
    // else {
    //     console.log("Not connected, cannot send message");
    //     res.end("Not connected, cannot send message");       
    // }        
});

function createJwt(projectId, privateKey, algorithm) {
    const token = {
      iat: parseInt(Date.now() / 1000),
      exp: parseInt(Date.now() / 1000) + 20 * 60, // 20 minutes
      aud: projectId,
    };
    var private_value = privateKey.replace(/\\n/g, '\n');
    return jwt.sign(token, private_value, {algorithm: algorithm});
}

app.listen(8080, () => console.log(`Service listening on port 8080!`));

